package com.example.a08activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        // 1
//
//        val writeTextMessage = this.findViewById<TextView>(R.id.write_text_message)
//        writeTextMessage.text = "Napíš nejaký text:"
//
//        val editableThing = this.findViewById<EditText>(R.id.editable_thing)
//        editableThing.hint = "Text"
//
//        // 2
//
//        val outputText = this.findViewById<TextView>(R.id.output_text)
//        val okButton = this.findViewById<Button>(R.id.ok_button)
//        okButton.setOnClickListener {
//            val upperCaseCheckBox = this.findViewById<CheckBox>(R.id.check_box_upper_case)
//            outputText.text = if (upperCaseCheckBox.isChecked) {
//                editableThing.text.toString().toUpperCase()
//            } else {
//                editableThing.text.toString().toLowerCase()
//            }
//        }
//    }

    // HW

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_hw)

        val editText1 = findViewById<EditText>(R.id.edit_text_1)
        val editText2 = findViewById<EditText>(R.id.edit_text_2)
        val resultText = findViewById<TextView>(R.id.result_text)
        val checkButton = findViewById<TextView>(R.id.check_button)

        checkButton.setOnClickListener {
            if (editText1.text.toString() == editText2.text.toString()) {
                resultText.text = "Texty jsou stejne"
                resultText.setTextColor(Color.GREEN)
            } else {
                resultText.text = "Texty jsou ruzne"
                resultText.setTextColor(Color.RED)
            }
        }
    }

}
